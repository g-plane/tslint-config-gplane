# tslint-config-gplane

This is shareable TSLint config created by [GPlane](https://github.com/g-plane).
It is used in GPlane's projects.

## Usage

Edit your `tslint.json` like this:

```json
{
  "extends": "tslint-config-gplane"
}
```

## Main Code Style

- No semi.
- 2 spaces indent.
- Single quote.
- No `var` keywords and prefer using `const`.
